
const path = require('path')
const fs = require('fs')

const xml = require('fast-xml-parser')

function trimExtension(filename) {
	const { dir, name } = path.parse(filename);
	return path.join(dir, name);
}

function minMax(l) {
    return {
        min: Math.min(...l),
        max: Math.max(...l)
    }
}

const inputFile = process.argv[2]
const outputFile = trimExtension(inputFile) + '.svg'
const lineWidth = '8px'
const boxInflate = 0.2
const imageWidth = 800

const gpxRaw = fs.readFileSync(inputFile).toString()
const gpx = xml.parse(gpxRaw, { ignoreAttributes: false, ignoreNameSpace: true })

const points = gpx.gpx.trk.trkseg.trkpt

// Extract lons & lats
const lon0 = parseFloat(points[0]["@_lon"])
const lat0 = parseFloat(points[0]["@_lat"])
const k = 100000
const coords = points.map(point => ({
	lon: parseFloat((point["@_lon"] - lon0)*k), 
	lat: parseFloat((point["@_lat"] - lat0)*k)
        }));

const { min: minLon, max: maxLon } = minMax(coords.map(c => c.lon));
const { min: minLat, max: maxLat } = minMax(coords.map(c => c.lat));
const lonSize = maxLon - minLon;
const latSize = maxLat - minLat;

const boxMinX = minLon - lonSize*boxInflate/2;
const boxMinY = minLat - latSize*boxInflate/2;
const boxWidth = lonSize * (1 + boxInflate);
const boxHeight = latSize * (1 + boxInflate);

//const aspect = imageWidth/imageHeight;
//const imageHeight = imageWidth/aspect;

const lineCoords = coords.map(c => `${c.lon},${c.lat}`).join(' ')

const svg = `<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<svg xmlns="http://www.w3.org/2000/svg" width="${imageWidth}px" viewBox="${boxMinX} ${boxMinY} ${boxWidth} ${boxHeight}">
<polyline points="${lineCoords}" stroke="black" stroke-width="${lineWidth}" fill="none"/>
</svg>`
fs.writeFileSync(outputFile, svg)



